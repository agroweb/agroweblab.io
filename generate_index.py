from string import Template

# template de lectura y archivo de salida
index_output  = open("web/index.html", "w")
template_html = open("web/index_template.html", "r").read()
template_tmp  = Template(template_html)

# figuras generadas con plotly y purgadas por ahora a mano -> nada mas que sacar el html y body
DIR = "/home/emiliano/data/dev/pysuelo/viz"
precipitacion = open(f"{DIR}/lluvia.html", "r").read()
#napa_freatica = open(f"{DIR}/nf.html", "r").read()
napa_freatica = open(f"{DIR}/lluvia_manual_nf.html", "r").read()
temperatura   = open(f"{DIR}/temperaturas.html", "r").read()
humedad       = open(f"{DIR}/humedad.html", "r").read()
humedad_suelo = open(f"{DIR}/hs.html", "r").read()
cultivos      = open(f"{DIR}/alfalfa.html", "r").read()
rendi_alfalfa = open(f"{DIR}/rendimiento_alfalfa.html", "r").read()
rendi_maiz    = open(f"{DIR}/rendimiento_maiz.html", "r").read()
alt_alfalfa   = open(f"{DIR}/altura_alfalfa_manual.html", "r").read()
alt_maiz      = open(f"{DIR}/altura_maiz_manual.html", "r").read()
velo_viento   = open(f"{DIR}/viento.html", "r").read()
baterias      = open(f"{DIR}/baterias.html", "r").read()


index_content = template_tmp.substitute(lluvia           = precipitacion,
                                        nf               = napa_freatica,
                                        temperatura      = temperatura,
                                        humedad_ambiente = humedad,
                                        hs               = humedad_suelo,
                                        alfalfa          = cultivos,
                                        viento           = velo_viento,
                                        baterias         = baterias,
                                        rendi_alfalfa    = rendi_alfalfa, 
                                        alt_alfalfa      = alt_alfalfa, 
                                        rendi_maiz       = rendi_maiz,
                                        alt_maiz         = alt_maiz
                                        )

index_output.write(index_content)
index_output.close()